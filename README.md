# What LibreWolf is not

* LibreWolf does **not** provide _[Anonymity](https://en.wikipedia.org/wiki/Anonymous_web_browsing)_, for that, use:
> [Tor Browser](https://www.torproject.org/).

_What is it then?_ It's a **_hardened_** _version_ of Firefox. (See bottom of file.)

# LibreWolf for windows

* **[download latest release](https://gitlab.com/librewolf-community/browser/windows/-/releases)**.
* [issue tracker](https://gitlab.com/librewolf-community/browser/windows/-/issues).

# download locations

* [[arch releases](https://gitlab.com/librewolf-community/browser/arch/-/releases)] - [librewolf-bin AUR(en)](https://aur.archlinux.org/packages/librewolf-bin/), [librewolf AUR(en)](https://aur.archlinux.org/packages/librewolf/)
* [[linux releases](https://gitlab.com/librewolf-community/browser/linux/-/releases)] - Flatpak, AppImage, Arch and other Linux builds of the LibreWolf browser.

# settings repository

* [[settings repository](https://gitlab.com/librewolf-community/settings)] - all _librewolf.cfg_ stuff.. _([issues](https://gitlab.com/librewolf-community/settings/-/issues))_

# librewolf repositories

* [[common](https://gitlab.com/librewolf-community/browser/common)] -> _([issues](https://gitlab.com/librewolf-community/browser/common/-/issues))_
* [[linux](https://gitlab.com/librewolf-community/browser/linux)] -> _([issues](https://gitlab.com/librewolf-community/browser/linux/-/issues))_
* [[website](https://gitlab.com/librewolf-community/librewolf-community.gitlab.io)] - _([issues](https://gitlab.com/librewolf-community/librewolf-community.gitlab.io/-/issues))_

# distro repositories

* [[arch](https://gitlab.com/librewolf-community/browser/arch)] -> _([issues](https://gitlab.com/librewolf-community/browser/arch/-/issues))_
* [[debian](https://gitlab.com/librewolf-community/browser/debian)] -> _([issues](https://gitlab.com/librewolf-community/browser/debian/-/issues))_
* [[flatpack](https://gitlab.com/librewolf-community/browser/flatpak)] -> _([issues](https://gitlab.com/librewolf-community/browser/flatpak/-/issues))_
* [[gentoo](https://gitlab.com/librewolf-community/browser/gentoo)] -> _([issues](https://gitlab.com/librewolf-community/browser/gentoo/-/issues))_
* [[macos](https://gitlab.com/librewolf-community/browser/macos)] -> _([issues](https://gitlab.com/librewolf-community/browser/macos/-/issues))_
* [[windows](https://gitlab.com/librewolf-community/browser/windows)] -> _([issues](https://gitlab.com/librewolf-community/browser/windows/-/issues))_

# community links

* If at all possible, pleasse go to the relevant 'issues' link and start discussing thing there.
* [[reddit](https://www.reddit.com/r/LibreWolf/)] - [r/LibreWolf](https://www.reddit.com/r/LibreWolf/) 😺
* [readthedocs.io](https://librewolf.readthedocs.io/en/latest/).
* [[gitter](https://gitter.im/librewolf-community/librewolf)], and the same room on [matrix](https://app.element.io/#/room/#librewolf-community_librewolf:gitter.im) (element.io).

# hardening firefox

* **tools**: [[deviceinfo.me](https://www.deviceinfo.me/)], [[coveryourtracks](https://coveryourtracks.eff.org/learn)], [[privacytools.io](https://privacytools.io/)], 
* eff.org: [surveillance self-defense](https://ssd.eff.org/en).
* Interesting wikipedia articles: [Browser fingerprinting](https://en.wikipedia.org/wiki/Device_fingerprint), [Browser security](https://en.wikipedia.org/wiki/Browser_security), [Browser exploit](https://en.wikipedia.org/wiki/Browser_exploit), [Anonymous web browsing](https://en.wikipedia.org/wiki/Anonymous_web_browsing), [Internet privacy](https://en.wikipedia.org/wiki/Internet_privacy).
